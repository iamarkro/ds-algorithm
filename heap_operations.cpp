#include <iostream>
#include <climits>

using namespace std;


class MinHeap
{

private:
	int capacity;
	int* heap_array;
	int heap_size;

public:
	MinHeap(int cap);
	int extract_min();
	void min_heapify(int index);
	void decrease_key(int index, int new_value);
	void delete_key(int index);
	int get_parent(int index);
	int left_child(int index);
	int right_child(int index);
	void swap(int* val_1, int* val_2);
	void insert_key(int value);
	void fix_heap(int index);
	void print_heap();
};


MinHeap::MinHeap(int cap)
{
	//cout<<"Heap initialized with capacity: "<<cap<<endl;
	heap_size=0;
	capacity= cap;
	heap_array= new int[capacity];
}

void MinHeap::decrease_key(int index, int new_value)
//decrease value at index to new_value (lesser than previous value)
{
	//cout<<"called decrease_key at index: "<<index<<endl;
	heap_array[index]= new_value;
	//print_heap();
	fix_heap(index);
	//cout<<"after fixing heap at decrease_key ..."<<endl;
	//print_heap();
}


int MinHeap::left_child(int index)
{
	return 2*index+1;
}

int MinHeap::right_child(int index)
{
	return 2*index+2;
}

void MinHeap::insert_key(int value)
{
	if(heap_size== capacity)
	{
		cout<<"Overflow...Can't insert another element in the heap"<<endl;
		return;
	}
	else
	{
		int i= heap_size;
		heap_array[i]= value;
		heap_size++;
		//cout<<"inserting "<<value<<" at index: "<<i<<" need to call fix_heap("<<i<<")"<<endl;
		fix_heap(i);
	}
	//print_heap();
}

int MinHeap::extract_min()
{
	//cout<<"called extract_min...."<<endl;
	if(heap_size==0)
	{
		cout<<" No element is present on the heap"<<endl;
		return INT_MAX;
	}

	else if(heap_size==1)
	{
		heap_size--;
		return heap_array[0];
	}

	else
	{
		//store the minimum value ie, root
		//swap the last element on the heap_array with the root
		//call min_heapify(0)
		//return root

		int root= heap_array[0];
		heap_array[0]= heap_array[heap_size-1];
		heap_size--;
		min_heapify(0);

		return root;

	}

}

void MinHeap::min_heapify(int index)
{
	//heapify a sub-tree with root at given index
	// assumption that the subtrees are already heapified

	int left= left_child(index);
	int right= right_child(index);
	int smallest= index;

	if(left < heap_size && heap_array[left] < heap_array[smallest])
		smallest= left;
	if(right < heap_size && heap_array[right] < heap_array[smallest])
		smallest= right;

	if(smallest != index)
	{
		swap(&heap_array[smallest], & heap_array[index]);
		min_heapify(smallest);
	}

}


void MinHeap::delete_key(int index)
{
	//cout<<"deleting key at index: "<<index<<" val: "<<heap_array[index]<<endl;
	decrease_key(index,INT_MIN);
	extract_min();
	//print_heap();

}


void MinHeap::fix_heap(int index)
//fixes the min-heap property
{
	while(index!=0 && heap_array[get_parent(index)] > heap_array[index] )
	{
		//cout<<"swap "<< heap_array[get_parent(index)] <<" with "<<heap_array[index]<<endl;
		swap(&heap_array[get_parent(index)],&heap_array[index]);
		index= get_parent(index);
	}
}

int MinHeap::get_parent(int index)
{
	return (index-1)/2;
}

void MinHeap::print_heap()
{
	for(int i=0;i<heap_size;i++)
		cout<<heap_array[i]<<" ";
	cout<<endl;
}

void MinHeap::swap(int* val_1,int* val_2)
{
	int temp = *val_1;
	*val_1= *val_2;
	*val_2= temp;
}


//test the heap
int main(void)
{
	MinHeap* heap= new MinHeap(10);
	heap->insert_key(5);
	heap->insert_key(2);
	heap->insert_key(1);
	heap->insert_key(3);
	heap->insert_key(6);
	heap->insert_key(4);
	heap->insert_key(7);

	heap->print_heap();

	heap->delete_key(3);
}

